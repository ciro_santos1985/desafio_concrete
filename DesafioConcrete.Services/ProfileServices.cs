﻿
using System;
using System.Threading.Tasks;
using DesafioConcrete.Services.Contracts;
using DesafioConcrete.Services.Contracts.Messages.Profile;
using DesafioConcrete.Data.Contracts;
using DesafioConcrete.Business.Model;
using System.Linq;
using DesafioConcrete.Business.ForProfile.Commands;
using DesafioConcrete.Practices.Commands;
using DesafioConcrete.Services.Contracts.Dtos;

namespace DesafioConcrete.Services
{
    public class ProfileServices : IProfileServices
    {
        private IDesafioConcreteUnitOfWork _unitOfWork;

        public ProfileServices(IDesafioConcreteUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _unitOfWork = unitOfWork;
        }

        private bool TryMapProfileFromRequest(SignUpProfileRequestMessage requestMessage, out Profile profile, out string failMessage)
        {
            try
            {
                if (requestMessage == null)
                {
                    profile = null;
                    failMessage = "Dados da requisição inválidos";
                    return false;
                }

                var auxProfile = new Profile()
                {
                    Email = requestMessage.Email,
                    Name = requestMessage.Nome,
                };

                auxProfile.ChangePassword(requestMessage.Senha);
                var phoneList = requestMessage.Telefones.ToList();

                foreach (var phoneDto in phoneList)
                {
                    auxProfile.AddPhoneNumber(new PhoneNumber(auxProfile.Id, phoneDto.DDD, phoneDto.Numero));
                }

                profile = auxProfile;
                failMessage = null;
                return true;
            }
            catch (ArgumentException e)
            {
                profile = null;
                failMessage = e.Message;
                return false;
            }
        }

        private SignUpProfileResponseMessage MapProfileToResponse(Profile profile, Guid accessToken, int statusCode)
        {
            var serviceResponse = new SignUpProfileResponseMessage() { StatusCode = statusCode };

            serviceResponse.DataAtualizacao = profile.LastUpdateTime;
            serviceResponse.DataCriacao = profile.CreationTime;
            serviceResponse.Email = profile.Email;
            serviceResponse.Id = profile.Id;
            serviceResponse.Nome = profile.Name;
            serviceResponse.Token = accessToken;
            serviceResponse.UltimoLogin = profile.LastLoginTime;

            serviceResponse.Telefones = profile.PhoneNumberCollection.Select(p => new PhoneNumberDto()
            {
                DDD = p.AreaCode,
                Numero = p.Number
            });

            return serviceResponse;
        }

        public async Task<SignUpProfileResponseMessage> SignUpProfileAsync(SignUpProfileRequestMessage requestMessage)
        {
            try
            {
                Profile profile = null;
                string failMessage = null;

                if (!TryMapProfileFromRequest(requestMessage, out profile, out failMessage))
                {
                    return new SignUpProfileResponseMessage()
                    {
                        StatusCode = 400,
                        Mensagem = failMessage
                    };
                }

                var signUpCommand = new SignupProfileCommand(profile, _unitOfWork.ProfileRepository.InsertAsync,
                    _unitOfWork.ProfileRepository.GetByEmailAsync);

                var commandResult = await signUpCommand.ExecuteAsync().ConfigureAwait(false);
                SignUpProfileResponseMessage serviceResponse = null;

                if (commandResult.Status == CommandResultStatus.CreatedResource)
                {
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    serviceResponse = MapProfileToResponse(profile, signUpCommand.NewAccessToken, (int)commandResult.Status);
                }
                else
                {
                    await _unitOfWork.RollbackAsync().ConfigureAwait(false);
                    serviceResponse = new SignUpProfileResponseMessage() { StatusCode = (int)commandResult.Status };
                }

                //pegando apenas a primeira mensagem para obedecer o contrato determinado no desafio
                if (commandResult.MessageCollection.Any())
                {
                    serviceResponse.Mensagem = commandResult.MessageCollection.First();
                }

                return serviceResponse;
            }
            catch (Exception e)
            {
                return new SignUpProfileResponseMessage()
                {
                    StatusCode = 500,
                    Mensagem = e.Message
                };
            }
        }

        public async Task<LoginProfileResponseMessage> LoginProfileAsync(LoginProfileRequestMessage requestMessage)
        {
            try
            {
                if (requestMessage == null)
                {
                    return new LoginProfileResponseMessage()
                    {
                        StatusCode = 400,
                        Mensagem = "Dados da requisição inválidos"
                    };
                }

                var loginCommand = new LoginProfileCommand(requestMessage.Email, requestMessage.Senha,
                    _unitOfWork.ProfileRepository.GetByEmailAsync,
                    _unitOfWork.ProfileRepository.UpdateAsync);

                var commandResult = await loginCommand.ExecuteAsync().ConfigureAwait(false);
                LoginProfileResponseMessage response = null;

                if (commandResult.Status == CommandResultStatus.Success)
                {
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    var signupResponse = MapProfileToResponse(loginCommand.Profile, loginCommand.AccessToken.Value, (int)commandResult.Status);
                    response = new LoginProfileResponseMessage(signupResponse);
                }
                else
                {
                    await _unitOfWork.RollbackAsync().ConfigureAwait(false);
                    response = new LoginProfileResponseMessage() { StatusCode = (int)commandResult.Status };
                }

                //pegando apenas a primeira mensagem para obedecer o contrato determinado no desafio
                if (commandResult.MessageCollection.Any())
                {
                    response.Mensagem = commandResult.MessageCollection.First();
                }

                return response;
            }
            catch (Exception e)
            {
                return new LoginProfileResponseMessage()
                {
                    StatusCode = 500,
                    Mensagem = e.Message
                };
            }
        }

        public async Task<GetProfileResponseMessage> GetProfileAsync(GetProfileRequestMessage requestMessage)
        {
            try
            {
                if (requestMessage == null || requestMessage.AccessToken == null || requestMessage.ProfileId == null)
                {
                    return new GetProfileResponseMessage()
                    {
                        StatusCode = 400,
                        Mensagem = "Dados da requisição inválidos"
                    };
                }

                var command = new GetProfileCommand(requestMessage.ProfileId.Value,
                    requestMessage.AccessToken.Value, _unitOfWork.ProfileRepository.GetByIdAsync);

                var commandResult = await command.ExecuteAsync().ConfigureAwait(false);
                GetProfileResponseMessage response = null;

                if (commandResult.Status == CommandResultStatus.Success)
                {
                    var signUpResponse = MapProfileToResponse(command.Profile, command.AccessToken, (int)commandResult.Status);
                    response = new GetProfileResponseMessage(signUpResponse);
                }
                else
                {
                    response = new GetProfileResponseMessage() { StatusCode = (int)commandResult.Status };
                }

                if (commandResult.MessageCollection.Any())
                {
                    response.Mensagem = commandResult.MessageCollection.First();
                }

                return response;
            }
            catch (Exception e)
            {
                return new GetProfileResponseMessage()
                {
                    StatusCode = 500,
                    Mensagem = e.Message
                };
            }
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
            _unitOfWork = null;
        }
    }
}
