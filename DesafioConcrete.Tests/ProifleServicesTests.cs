﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesafioConcrete.Business.Model;
using DesafioConcrete.Data.Contracts;
using Moq;
using System.Threading.Tasks;
using DesafioConcrete.Services;
using DesafioConcrete.Services.Contracts.Messages.Profile;
using DesafioConcrete.Services.Contracts.Dtos;
using DesafioConcrete.Practices;
using System.Linq;

namespace DesafioConcrete.Tests
{
    [TestClass]
    public class ProifleServicesTests
    {
        [TestMethod]
        public async Task SignUpProfile_succes()
        {
            //arrange: mockando repositório (consulta pelo email);
            var email = "ciro.santos@teste.com";
            Profile persisted = null;

            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e == email)))
                .ReturnsAsync(null);

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e != email)))
                .Throws(new Exception("email passado ao repositório está errado"));

            //arrange: mockando repositório (inserção)
            mockRepository.Setup(r => r.InsertAsync(It.IsAny<Profile>()))
                .Returns<Profile>((profile) =>
                {
                    return Task.Run(() =>
                    {
                        persisted = profile;
                    });
                });

            //arrange: mockando unitOfWork
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            bool transactionCommited = false;

            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            mockUnit.Setup(u => u.CommitAsync())
                .Returns(() =>
                {
                    return Task.Run(() =>
                    {
                        if (persisted == null)
                            throw new Exception("erro na chamada do commit");

                        transactionCommited = true;
                    });
                });

            //act: executando serviço
            var request = new SignUpProfileRequestMessage()
            {
                Email = email,
                Nome = "Ciro Teste",
                Senha = "testesenha",
                Telefones = new PhoneNumberDto[] { new PhoneNumberDto() { DDD = "21", Numero = "874563545" } }
            };

            var profileService = new ProfileServices(mockUnit.Object);
            var response = await profileService.SignUpProfileAsync(request);

            //assert:
            Assert.IsTrue(transactionCommited);
            Assert.IsNotNull(response.Token);
            Assert.AreNotEqual(Guid.Empty, response.Token);

            var expectedResponse = new SignUpProfileResponseMessage()
            {
                DataCriacao = persisted.CreationTime,
                Email = request.Email,
                Id = persisted.Id,
                Nome = request.Nome,
                StatusCode = 201,
                Telefones = request.Telefones,
                Token = response.Token,
            };

            expectedResponse.DataAtualizacao = expectedResponse.DataCriacao;
            expectedResponse.UltimoLogin = expectedResponse.DataCriacao;

            response.Telefones = response.Telefones.ToArray();
            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task SignUpProfile_should_fail_email_violation()
        {
            //arrange: mockando repositório (consulta pelo email);
            var email = "email_duplicated@teste.com";
            Profile persisted = new Profile() { Email = email };

            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e == email)))
                .ReturnsAsync(persisted);

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e != email)))
                .Throws(new Exception("email passado ao repositório está errado"));

            //arrange: mockando unitOfWork
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            var rollbackTran = false;

            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);
            mockUnit.Setup(u => u.RollbackAsync()).Returns(Task.Run(() => { rollbackTran = true; }));

            //act: executando serviço
            var request = new SignUpProfileRequestMessage()
            {
                Email = email,
                Nome = "Ciro Teste",
                Senha = "testesenha",
                Telefones = new PhoneNumberDto[] { new PhoneNumberDto() { DDD = "21", Numero = "874563545" } }
            };

            var profileService = new ProfileServices(mockUnit.Object);
            var response = await profileService.SignUpProfileAsync(request);

            //assert:
            Assert.IsTrue(rollbackTran);

            var expectedResponse = new SignUpProfileResponseMessage()
            {
                DataCriacao = null,
                Email = null,
                Id = null,
                Nome = null,
                StatusCode = 400,
                Telefones = new PhoneNumberDto[0],
                Token = null,
                DataAtualizacao = null,
                UltimoLogin = null,
                Mensagem = "Email já cadastrado"
            };

            response.Telefones = response.Telefones.ToArray();
            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task LoginProfile_success()
        {
            //arrange: mock repositório profile (registro existente)
            var senha = "teste";
            var persisted = new Profile(Guid.NewGuid(), DateTime.Now.AddMinutes(-10));
            persisted.Email = "login@teste.com";
            persisted.Name = "login teste";
            persisted.LastLoginTime = persisted.CreationTime;
            persisted.LastUpdateTime = persisted.CreationTime;

            persisted.ChangePassword(senha);
            persisted.AddPhoneNumber(new PhoneNumber(persisted.Id, "21", "4354484646"));

            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e == persisted.Email)))
                .ReturnsAsync(persisted);

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e != persisted.Email)))
                .Throws(new Exception("email errado passado ao repositório"));

            //arrange: mock repositório profile (atualização)
            var commit = false;
            Profile updated = null;

            mockRepository.Setup(r => r.UpdateAsync(It.Is<Profile>(p => p.Id != persisted.Id)))
                .Throws(new Exception("erro na chamada do update"));

            mockRepository.Setup(r => r.UpdateAsync(It.Is<Profile>(p => p.Id == persisted.Id)))
                .Returns<Profile>((profile) =>
                {
                    return Task.Run<bool>(() =>
                    {
                        updated = profile;
                        return true;
                    });
                });

            //arrange: mock do unitOfWork;
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            mockUnit.Setup(u => u.CommitAsync())
                .Returns(() =>
                {
                    return Task.Run(() =>
                    {
                        if (updated == null)
                            throw new Exception("erro na chamada do commit");

                        commit = true;
                    });
                });

            //act
            var request = new LoginProfileRequestMessage() { Email = persisted.Email, Senha = senha };
            var service = new ProfileServices(mockUnit.Object);
            var response = await service.LoginProfileAsync(request);

            //arrange;
            Assert.IsTrue(commit);
            Assert.IsNotNull(response.Token);
            Assert.AreNotEqual(Guid.Empty, response.Token);

            var expectedResponse = new LoginProfileResponseMessage()
            {
                DataCriacao = persisted.CreationTime,
                Email = persisted.Email,
                Id = persisted.Id,
                Nome = persisted.Name,
                StatusCode = 200,
                Token = response.Token,
                DataAtualizacao = updated.LastUpdateTime,
                UltimoLogin = updated.LastUpdateTime,
            };

            var phoneList = persisted.PhoneNumberCollection.Select(p => new PhoneNumberDto()
            {
                DDD = p.AreaCode,
                Numero = p.Number
            })
            .ToArray();

            expectedResponse.Telefones = phoneList;
            response.Telefones = response.Telefones.ToArray();

            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task LoginProfile_should_fail_wrong_password()
        {
            //arrange: mock repositório profile (registro existente)
            var senha = "teste";
            var persisted = new Profile(Guid.NewGuid(), DateTime.Now.AddMinutes(-10));

            persisted.Email = "errosenha@teste.com";
            persisted.ChangePassword(senha);
            
            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByEmailAsync(It.Is<string>(e => e == persisted.Email)))
                .ReturnsAsync(persisted);
           
            //arrange: mock do unitOfWork;
            var rollback = false;
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();

            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            mockUnit.Setup(u => u.RollbackAsync())
                .Returns(() =>
                {
                    return Task.Run(() =>
                    {
                        rollback = true;
                    });
                });

            //act
            var request = new LoginProfileRequestMessage() { Email = persisted.Email, Senha = "errada" };
            var service = new ProfileServices(mockUnit.Object);
            var response = await service.LoginProfileAsync(request);

            //arrange;
            Assert.IsTrue(rollback);

            var expectedResponse = new LoginProfileResponseMessage()
            {
                StatusCode = 401,
                Telefones = new PhoneNumberDto[0],
                Mensagem = "Usuário ou senha inválidos"
            };
                        
            response.Telefones = response.Telefones.ToArray();
            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task GetProfile_success()
        {
            //arrange: mock repositório profile (registro existente)
            var senha = "teste";
            var accessToken = Guid.NewGuid();

            var persisted = new Profile(Guid.NewGuid(), DateTime.Now.AddMinutes(-10));
            persisted.Email = "login@teste.com";
            persisted.Name = "login teste";
            persisted.LastLoginTime = persisted.CreationTime;
            persisted.LastUpdateTime = persisted.CreationTime;

            persisted.ChangeAccessToken(accessToken);
            persisted.ChangePassword(senha);
            persisted.AddPhoneNumber(new PhoneNumber(persisted.Id, "21", "4354484646"));

            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByIdAsync(It.Is<Guid>(id => id == persisted.Id)))
                .ReturnsAsync(persisted);

            //arrange: mock UnitOfWork
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            //act
            var request = new GetProfileRequestMessage() { AccessToken = accessToken, ProfileId = persisted.Id };
            var service = new ProfileServices(mockUnit.Object);
            var response = await service.GetProfileAsync(request).ConfigureAwait(false);

            //assert:
            var expectedResponse = new GetProfileResponseMessage()
            {
                DataAtualizacao = persisted.LastUpdateTime,
                DataCriacao = persisted.CreationTime,
                Email = persisted.Email,
                Id = persisted.Id,
                Nome = persisted.Name,
                StatusCode = 200,
                Token = accessToken,
                UltimoLogin = persisted.LastLoginTime
            };

            expectedResponse.Telefones = persisted.PhoneNumberCollection
                .Select(p => new PhoneNumberDto() { DDD = p.AreaCode, Numero = p.Number })
                .ToArray();

            response.Telefones = response.Telefones.ToArray();

            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task GetProfile_should_fail_expired_session()
        {
            //arrange: mock repositório profile (registro existente)
            var accessToken = Guid.NewGuid();
            var persisted = new Profile(Guid.NewGuid(), DateTime.Now.AddMinutes(-40));

            persisted.LastLoginTime = persisted.CreationTime;
            persisted.LastUpdateTime = persisted.CreationTime;

            persisted.ChangeAccessToken(accessToken);
            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByIdAsync(It.Is<Guid>(id => id == persisted.Id)))
                .ReturnsAsync(persisted);

            //arrange: mock UnitOfWork
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            //act
            var request = new GetProfileRequestMessage() { AccessToken = accessToken, ProfileId = persisted.Id };
            var service = new ProfileServices(mockUnit.Object);
            var response = await service.GetProfileAsync(request).ConfigureAwait(false);

            //assert:
            var expectedResponse = new GetProfileResponseMessage()
            {
                StatusCode = 401,
                Telefones = new PhoneNumberDto[0],
                Mensagem = "Sessão inválida"
            };
                   
            response.Telefones = response.Telefones.ToArray();
            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }

        [TestMethod]
        public async Task GetProfile_should_fail_wrong_token()
        {
            //arrange: mock repositório profile (registro existente)
            var accessToken = Guid.NewGuid();
            var persisted = new Profile(Guid.NewGuid(), DateTime.Now.AddMinutes(-5));

            persisted.LastLoginTime = persisted.CreationTime;
            persisted.LastUpdateTime = persisted.CreationTime;

            persisted.ChangeAccessToken(accessToken);
            var mockRepository = new Mock<IProfileRepository>();

            mockRepository.Setup(r => r.GetByIdAsync(It.Is<Guid>(id => id == persisted.Id)))
                .ReturnsAsync(persisted);

            //arrange: mock UnitOfWork
            var mockUnit = new Mock<IDesafioConcreteUnitOfWork>();
            mockUnit.SetupGet(u => u.ProfileRepository).Returns(mockRepository.Object);

            //act
            var request = new GetProfileRequestMessage() { AccessToken = Guid.NewGuid(), ProfileId = persisted.Id };
            var service = new ProfileServices(mockUnit.Object);
            var response = await service.GetProfileAsync(request).ConfigureAwait(false);

            //assert:
            var expectedResponse = new GetProfileResponseMessage()
            {
                StatusCode = 401,
                Telefones = new PhoneNumberDto[0],
                Mensagem = "Não autorizado"
            };

            response.Telefones = response.Telefones.ToArray();
            Assert.IsTrue(expectedResponse.IsEquivalentTo(response));
        }
    }
}
