﻿using System;
using DesafioConcrete.Business.Model;
using DesafioConcrete.Practices.Specification;
using System.Collections.Generic;
using DesafioConcrete.Practices.Text;
using System.Linq;

namespace Desafio.Business.ForProfile.Specs
{
    public class ValidToPersistProfileSpec : ISpecification<Profile>
    {
        private ValidToPersistPhoneNumberSpec _phoneNumberSpec = new ValidToPersistPhoneNumberSpec();

        public SpecificationResult IsSatisfiedBy(Profile profile)
        {
            if (profile == null)
                throw new ArgumentNullException("businessObject");

            var messageCollection = new List<string>();
            var isSatisfied = true;

            if (string.IsNullOrWhiteSpace(profile.Password))
                messageCollection.Add("Senha deve ser definida");

            if (string.IsNullOrWhiteSpace(profile.AccessToken))
                messageCollection.Add("Token de acesso deve ser definido");

            if (string.IsNullOrWhiteSpace(profile.Name))
                messageCollection.Add("Nome deve ser definido");

            if (profile.Name.Length > 256)
                messageCollection.Add("Nome deve conter no máximo 256 caracteres");

            if (profile.Email.Length > 256)
                messageCollection.Add("Email deve conter no máximo 256 caracteres");

            if (!profile.Email.IsValidEmail())
                messageCollection.Add("Email inválido");

            if (!profile.PhoneNumberCollection.Any())
                messageCollection.Add("Ao menos um telefone deve ser cadastrado");

            isSatisfied = messageCollection.Count == 0;

            foreach (var phone in profile.PhoneNumberCollection)
            {
                var phoneSpecResult = _phoneNumberSpec.IsSatisfiedBy(phone);

                if (!phoneSpecResult.IsSatisfied)
                {
                    isSatisfied = false;
                    messageCollection.AddRange(phoneSpecResult.MessageCollection);
                }
            }

            return new SpecificationResult(isSatisfied, messageCollection);
        }
    }
}
