﻿using System;
using DesafioConcrete.Business.Model;
using DesafioConcrete.Practices.Specification;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Desafio.Business.ForProfile.Specs
{
    public class ValidToPersistPhoneNumberSpec : ISpecification<PhoneNumber>
    {
        public SpecificationResult IsSatisfiedBy(PhoneNumber phone)
        {
            if (phone == null)
                throw new ArgumentNullException("phone");

            var messageCollection = new List<string>();
            var regexOnlyNumber = new Regex(@"^\d+$");

            if (phone.AreaCode == null || !regexOnlyNumber.IsMatch(phone.AreaCode))
                messageCollection.Add("Código de área inválido");

            if (phone.AreaCode.Length > 5)
                messageCollection.Add("Código de área deve ter no máximo 5 dígitos.");

            if (phone.Number == null || !regexOnlyNumber.IsMatch(phone.Number))
                messageCollection.Add("Número de telefone inválido");

            if (phone.Number.Length > 15)
                messageCollection.Add("Número de telefone deve ter no máximo 15 dígitos");

            bool isSatisfied = messageCollection.Count == 0;
            return new SpecificationResult(isSatisfied, messageCollection);
        }
    }
}
