﻿using System;
using System.Threading.Tasks;
using DesafioConcrete.Practices.Commands;
using DesafioConcrete.Business.Model;
using Desafio.Business.ForProfile.Specs;

namespace DesafioConcrete.Business.ForProfile.Commands
{
    public class SignupProfileCommand : IAsyncCommand
    {
        private Func<Profile, Task> _persistAsyncHandler;
        private Func<string, Task<Profile>> _getByEmailAsyncHandler;

        public Profile NewProfile { get; private set; }
        public Guid NewAccessToken { get; private set; }

        public SignupProfileCommand(Profile profile, Func<Profile, Task> persistAsyncHandler, Func<string, Task<Profile>> getByEmailAsyncHandler)
        {
            if (persistAsyncHandler == null)
                throw new ArgumentNullException("persistHandler");

            if (getByEmailAsyncHandler == null)
                throw new ArgumentNullException("getByEmailAsyncHandler");

            if (profile == null)
                throw new ArgumentNullException("profile");

            NewProfile = profile;
            _persistAsyncHandler = persistAsyncHandler;
            _getByEmailAsyncHandler = getByEmailAsyncHandler;
        }

        public CommandResult Execute()
        {
            return ExecuteAsync().Result;
        }

        public async Task<CommandResult> ExecuteAsync()
        {
            var profileSpec = new ValidToPersistProfileSpec();
            var newToken = Guid.NewGuid();

            NewProfile.ChangeAccessToken(newToken);
            NewProfile.LastUpdateTime = NewProfile.CreationTime;
            NewProfile.LastLoginTime = NewProfile.CreationTime;

            var specResult = profileSpec.IsSatisfiedBy(NewProfile);

            if (!specResult.IsSatisfied)
            {
                return new CommandResult(CommandResultStatus.InvalidInputs, specResult.MessageCollection);
            }

            var persisted = await _getByEmailAsyncHandler.Invoke(NewProfile.Email).ConfigureAwait(false);

            if (persisted != null)
            {
                var failResult = new CommandResult(CommandResultStatus.InvalidInputs);
                failResult.AddMessage("Email já cadastrado");

                return failResult;
            }

            await _persistAsyncHandler.Invoke(NewProfile).ConfigureAwait(false);
            NewAccessToken = newToken;

            return new CommandResult(CommandResultStatus.CreatedResource);
        }
    }
}
