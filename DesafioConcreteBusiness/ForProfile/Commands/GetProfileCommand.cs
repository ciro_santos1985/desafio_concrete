﻿

using System;
using System.Threading.Tasks;
using DesafioConcrete.Practices.Commands;
using DesafioConcrete.Business.Model;

namespace DesafioConcrete.Business.ForProfile.Commands
{
    public class GetProfileCommand : IAsyncCommand
    {
        private Func<Guid, Task<Profile>> _getByProfileByIdHandler;

        public Guid ProfileId { get; private set; }

        public Guid AccessToken { get; private set; }

        public Profile Profile { get; private set; }

        public GetProfileCommand(Guid profileId, Guid accessToken, Func<Guid,Task<Profile>> getByProfileByIdHandler)
        {
            if (getByProfileByIdHandler == null)
                throw new ArgumentNullException("getByProfileByIdHandler");

            ProfileId = profileId;
            AccessToken = accessToken;
            _getByProfileByIdHandler = getByProfileByIdHandler;
        }

        public CommandResult Execute()
        {
            return ExecuteAsync().Result;
        }

        public async Task<CommandResult> ExecuteAsync()
        {
            var startAt = DateTime.Now;
            var persisted = await _getByProfileByIdHandler.Invoke(ProfileId).ConfigureAwait(false);

            if(persisted == null)
            {
                return new CommandResult(CommandResultStatus.ResourceNotFound, "Perfil não encontrado");
            }

            var expirationTime = persisted.LastLoginTime.AddMinutes(30);

            if (!persisted.MatchAccessToken(AccessToken))
            {
                return new CommandResult(CommandResultStatus.Unauthorized, "Não autorizado");
            }

            if (expirationTime.CompareTo(startAt) < 0)
            {
                return new CommandResult(CommandResultStatus.Unauthorized, "Sessão inválida");
            }

            Profile = persisted;
            return new CommandResult(CommandResultStatus.Success);
        }
    }
}
