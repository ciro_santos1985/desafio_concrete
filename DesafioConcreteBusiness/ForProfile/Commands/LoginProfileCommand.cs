﻿using System;
using System.Threading.Tasks;
using DesafioConcrete.Practices.Commands;
using DesafioConcrete.Business.Model;

namespace DesafioConcrete.Business.ForProfile.Commands
{
    public class LoginProfileCommand : IAsyncCommand
    {
        private string _senha;

        private Func<string, Task<Profile>> _getByEmailHandler;

        private Func<Profile, Task> _updateProfileHandler;

        public string Email { get; private set; }

        public Guid? AccessToken { get; private set; }

        public Profile Profile { get; private set; }

        public LoginProfileCommand(string email, string senha, Func<string, Task<Profile>> getByEmailHandler, Func<Profile, Task> updateProfileHandler)
        {
            if (getByEmailHandler == null)
                throw new ArgumentNullException("getByEmailHandler");

            if (updateProfileHandler == null)
                throw new ArgumentNullException("updateProfileHandler");

            _senha = senha;
            _getByEmailHandler = getByEmailHandler;

            _updateProfileHandler = updateProfileHandler;
            Email = email;
        }

        public CommandResult Execute()
        {
            return ExecuteAsync().Result;
        }

        public async Task<CommandResult> ExecuteAsync()
        {
            var loginTime = DateTime.Now;
            var defaultError = new CommandResult(CommandResultStatus.Unauthorized, "Usuário ou senha inválidos");

            if (string.IsNullOrWhiteSpace(Email) || string.IsNullOrEmpty(_senha))
            {
                return defaultError;
            }

            var persisted = await _getByEmailHandler.Invoke(Email).ConfigureAwait(false);

            if (persisted == null || !persisted.MatchPassword(_senha))
            {
                return defaultError;
            }

            var generatedToken = Guid.NewGuid();
            persisted.ChangeAccessToken(generatedToken);

            persisted.LastLoginTime = loginTime;
            persisted.LastUpdateTime = loginTime;

            await _updateProfileHandler.Invoke(persisted).ConfigureAwait(false);

            AccessToken = generatedToken;
            Profile = persisted;

            return new CommandResult(CommandResultStatus.Success);
        }
    }
}
