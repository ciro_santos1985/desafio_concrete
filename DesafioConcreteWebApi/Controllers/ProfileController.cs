﻿using DesafioConcrete.Services.Contracts;
using DesafioConcrete.Services.Contracts.Messages.Profile;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DesafioConcreteWebApi.Controllers
{
    public class ProfileController : ApiController
    {
        private IProfileServices _profileServices;

        public ProfileController(IProfileServices profileServices)
            : base()
        {
            if (profileServices == null)
                throw new ArgumentNullException("profileServices");

            _profileServices = profileServices;
        }

        [HttpPost]
        [Route("api/profile")]
        public async Task<HttpResponseMessage> SignUpAsync(SignUpProfileRequestMessage requestMessage)
        {
            var serviceResponse = await _profileServices.SignUpProfileAsync(requestMessage).ConfigureAwait(false);
            return Request.CreateResponse((HttpStatusCode)serviceResponse.StatusCode, serviceResponse);
        }

        [HttpPut]
        [Route("api/profile/auth")]
        public async Task<HttpResponseMessage> LoginAsync(LoginProfileRequestMessage requestMessage)
        {
            /**
             * Passando as credenciais no corpo da requisição pois foi a forma solicitada requisitada.
             * a melhor prática seria passar as credenciais, estruturada em uma string com encode base64, 
             * no header Authorization com o realm Basic
            */
            var serviceResponse = await _profileServices.LoginProfileAsync(requestMessage).ConfigureAwait(false);
            return Request.CreateResponse((HttpStatusCode)serviceResponse.StatusCode, serviceResponse);
        }

        [HttpGet]
        [Route("api/profile/{profileId}")]
        public async Task<HttpResponseMessage> GetProfileAsync(Guid? profileId)
        {
            var header = Request.Headers.Authorization;
            Guid? accessToken = null;

            if (header != null || header.Scheme == "Bearer")
            {
                Guid tokenAux;

                if (Guid.TryParse(header.Parameter, out tokenAux))
                {
                    accessToken = tokenAux;
                }
            }

            var request = new GetProfileRequestMessage() { AccessToken = accessToken, ProfileId = profileId };
            var response = await _profileServices.GetProfileAsync(request).ConfigureAwait(false);

            return Request.CreateResponse((HttpStatusCode)response.StatusCode, response);            
        }

        public new void Dispose()
        {
            _profileServices.Dispose();
            _profileServices = null;
            base.Dispose();
        }
    }
}
