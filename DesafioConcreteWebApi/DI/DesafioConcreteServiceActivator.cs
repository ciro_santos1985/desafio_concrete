﻿
using DesafioConcrete.Data;
using DesafioConcrete.Services;
using DesafioConcreteWebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace DesafioConcreteWebApi.DI
{
    public class DesafioConcreteServiceActivator : IHttpControllerActivator
    {
        private static Dictionary<Type, Func<HttpRequestMessage, HttpControllerDescriptor, IHttpController>> _controllerFactory;

        static DesafioConcreteServiceActivator()
        {
            _controllerFactory = new Dictionary<Type, Func<HttpRequestMessage, HttpControllerDescriptor, IHttpController>>();

            //ProfileController
            var createProfileController = new Func<HttpRequestMessage, HttpControllerDescriptor, IHttpController>((message, descriptor) =>
            {
                var unitOfWork = new EfSqliteUnitOfWork();
                var profileService = new ProfileServices(unitOfWork);

                var controller = new ProfileController(profileService);
                controller.RequestContext = message.GetRequestContext();
                controller.ControllerContext.ControllerDescriptor = descriptor;
                controller.Request = message;

                return controller;
            });

            _controllerFactory.Add(typeof(ProfileController), createProfileController);
        }

        public DesafioConcreteServiceActivator()
           : base()
        {
            
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            if (_controllerFactory.ContainsKey(controllerType))
            {
                return _controllerFactory[controllerType].Invoke(request, controllerDescriptor);
            }

            return null;
        }
    }
}