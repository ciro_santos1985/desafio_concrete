﻿

using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Dtos
{
    [DataContract]
    public class PhoneNumberDto
    {
        [DataMember(Name ="numero")]
        public string Numero { get; set; }

        [DataMember(Name ="ddd")]
        public string DDD { get; set; }
    }
}
