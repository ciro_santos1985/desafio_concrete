﻿using DesafioConcrete.Services.Contracts.Messages.Profile;
using System;
using System.Threading.Tasks;

namespace DesafioConcrete.Services.Contracts
{
    public interface IProfileServices : IDisposable
    {
        Task<SignUpProfileResponseMessage> SignUpProfileAsync(SignUpProfileRequestMessage requestMessage);

        Task<LoginProfileResponseMessage> LoginProfileAsync(LoginProfileRequestMessage requestMessage);

        Task<GetProfileResponseMessage> GetProfileAsync(GetProfileRequestMessage requestMessage);
    }
}
