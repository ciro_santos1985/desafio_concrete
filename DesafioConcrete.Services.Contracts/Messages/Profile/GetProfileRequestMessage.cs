﻿
using System;
using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Messages.Profile
{
    [DataContract]
    public class GetProfileRequestMessage
    {
        [DataMember]
        public Guid? AccessToken { get; set; }

        [DataMember]
        public Guid? ProfileId { get; set; }
    }
}
