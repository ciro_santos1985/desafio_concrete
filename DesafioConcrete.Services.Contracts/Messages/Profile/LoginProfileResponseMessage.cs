﻿
using DesafioConcrete.Services.Contracts.Dtos;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Messages.Profile
{
    [DataContract]
    public class LoginProfileResponseMessage : BaseResponseMessage
    {
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "telefones")]
        public IEnumerable<PhoneNumberDto> Telefones { get; set; }

        [DataMember(Name = "id")]
        public Guid? Id { get; set; }

        [DataMember(Name = "data_criacao")]
        public DateTime? DataCriacao { get; set; }

        [DataMember(Name = "data_atualizacao")]
        public DateTime? DataAtualizacao { get; set; }

        [DataMember(Name = "ultimo_login")]
        public DateTime? UltimoLogin { get; set; }

        [DataMember(Name = "token")]
        public Guid? Token { get; set; }

        public LoginProfileResponseMessage()
        {
            Telefones = new List<PhoneNumberDto>();
        }

        public LoginProfileResponseMessage(SignUpProfileResponseMessage signUpResponse)
        {
            if (signUpResponse == null)
                throw new ArgumentNullException("signUpResponse");

            StatusCode = signUpResponse.StatusCode;
            Mensagem = signUpResponse.Mensagem;
            Nome = signUpResponse.Nome;
            Email = signUpResponse.Email;
            Telefones = signUpResponse.Telefones;
            Id = signUpResponse.Id;
            DataCriacao = signUpResponse.DataCriacao;
            DataAtualizacao = signUpResponse.DataAtualizacao;
            UltimoLogin = signUpResponse.UltimoLogin;
            Token = signUpResponse.Token;
        }
    }
}
