﻿
using DesafioConcrete.Services.Contracts.Dtos;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Messages.Profile
{
    [DataContract]
    public class SignUpProfileRequestMessage
    {
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "senha")]
        public string Senha { get; set; }

        [DataMember(Name = "telefones")]
        public IEnumerable<PhoneNumberDto> Telefones { get; set; }
    }
}
