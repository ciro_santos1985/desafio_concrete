﻿
using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Messages.Profile
{
    [DataContract]
    public class LoginProfileRequestMessage
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "senha")]
        public string Senha { get; set; }
    }
}
