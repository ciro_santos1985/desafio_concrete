﻿
using System.Runtime.Serialization;

namespace DesafioConcrete.Services.Contracts.Messages
{
    [DataContract]
    public class BaseResponseMessage
    {
        [DataMember(Name = "statusCode")]
        public int StatusCode { get; set; }

        [DataMember(Name = "mensagem")]
        public string Mensagem { get; set; }
    }
}
