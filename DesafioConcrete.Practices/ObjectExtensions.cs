﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace DesafioConcrete.Practices
{
    public static class ObjectExtensions
    {
        private static Type _dateTimeTypeOf = typeof(DateTime);
        private static Type _stringTypeOf = typeof(string);
        private static Type _uriTypeOf = typeof(Uri);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns></returns>
        private static bool IsEquivalentBasic(object expected, object actual)
        {
            if (expected == null && actual == null)
                return true;

            if (expected == null || actual == null)
                return false;

            Type expectedPropertyValueType = expected.GetType();

            if (expectedPropertyValueType.Equals(_dateTimeTypeOf))
            {
                //comparação da data irá considerar apenas até o segundo
                DateTime expectedValueAsDate = (DateTime)expected;
                expectedValueAsDate = new DateTime(expectedValueAsDate.Year, expectedValueAsDate.Month, expectedValueAsDate.Day, expectedValueAsDate.Hour, expectedValueAsDate.Minute, expectedValueAsDate.Second);

                DateTime actualValueAsDate = (DateTime)actual;
                actualValueAsDate = new DateTime(actualValueAsDate.Year, actualValueAsDate.Month, actualValueAsDate.Day, actualValueAsDate.Hour, actualValueAsDate.Minute, actualValueAsDate.Second);

                return expectedValueAsDate.CompareTo(actualValueAsDate) == 0;
            }
            else
            {
                return object.Equals(expected, actual);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        private static bool IsEquivalentArray(object[] expectedArray, object[] actualArray, bool checkNonPublicMembers)
        {
            // se o length não bater não são equivalentes
            if (expectedArray.Length != actualArray.Length)
            {
                return false;
            }

            //comparando cada item da coleção
            for (int i = 0; i < expectedArray.Length; i++)
            {
                if (!IsEquivalentTo(expectedArray[i], actualArray[i], checkNonPublicMembers))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns></returns>
        private static bool IsEquivalentDictionary(IDictionary expected, IDictionary actual, bool checkNonPublicMembers)
        {
            List<object> expectedKeyList = new List<object>();
            List<object> expectedValueList = new List<object>();

            List<object> actualKeyList = new List<object>();
            List<object> actualValueList = new List<object>();


            Task loadExpectedTask = Task.Run(() =>
            {
                foreach (DictionaryEntry item in expected)
                {
                    expectedKeyList.Add(item.Key);
                    expectedValueList.Add(item.Value);
                };
            });

            Task loadActualTask = Task.Run(() =>
            {
                foreach (DictionaryEntry item in actual)
                {
                    actualKeyList.Add(item.Key);
                    actualValueList.Add(item.Value);
                };
            });

            Task.WhenAll(new Task[] { loadExpectedTask, loadActualTask }).Wait();

            if (!IsEquivalentTo(expectedKeyList, actualKeyList, checkNonPublicMembers))
            {
                return false;
            }

            if (!IsEquivalentTo(expectedValueList, actualValueList, checkNonPublicMembers))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns></returns>
        public static bool IsEquivalentTo(this object expected, object actual, bool checkNonPublicMembers = false)
        {
            //verificando se foram passadas referências nulas
            if (expected == null && actual == null)
                return true;

            if (expected == null || actual == null)
                return false;

            //obervando equivalência dos tipos de objetos passados
            Type expectedType = expected.GetType();
            Type actualType = actual.GetType();

            if (!expectedType.AssemblyQualifiedName.Equals(actualType.AssemblyQualifiedName))
            {
                return false;
            }

            //se o valor da propriedade não for um tipo básico ou data ou uri
            if (expectedType.IsClass && !_stringTypeOf.Equals(expectedType) && !_uriTypeOf.Equals(expectedType))
            {
                //se for uma coleção vai comparar cada item da coleção
                if (expected is IEnumerable)
                {
                    if (expected is IDictionary)
                    {

                        IDictionary expectedDictionary = (IDictionary)expected;
                        IDictionary actualDictionary = (IDictionary)actual;

                        if (!IsEquivalentDictionary(expectedDictionary, actualDictionary, checkNonPublicMembers))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //transformando os valores a serem comparados para array
                        IEnumerable expectedCollection = (IEnumerable)expected;
                        IEnumerable actualCollection = (IEnumerable)actual;

                        List<object> expectedList = new List<object>();
                        List<object> actualList = new List<object>();

                        Task loadExpectedTask = Task.Run(() => 
                        {
                            foreach (object itemExpected in expectedCollection)
                            {
                                expectedList.Add(itemExpected);
                            }
                        });

                        Task loadActualTask = Task.Run(() => 
                        {
                            foreach (object actualItem in actualCollection)
                            {
                                actualList.Add(actualItem);
                            }
                        });

                        Task.WhenAll(new Task[] { loadExpectedTask, loadActualTask }).Wait();

                        if (!IsEquivalentArray(expectedList.ToArray(), actualList.ToArray(), checkNonPublicMembers))
                        {
                            return false;
                        }
                    }
                }

                // se a comparação for entre tipos complexos
                else
                {
                    //obtendo as propriedades do tipo passado
                    PropertyInfo[] properties = null;

                    if (checkNonPublicMembers)
                    {
                        properties = expectedType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    }
                    else
                    {
                        properties = expectedType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    }

                    //comparando o valor das propriedades de instância
                    for (int i = 0; i < properties.Length; i++)
                    {
                        //obtendo o valor das propriedades
                        object expectedPropertyValue = properties[i].GetValue(expected);
                        object actualPropertyValue = properties[i].GetValue(actual);

                        if (!IsEquivalentTo(expectedPropertyValue, actualPropertyValue))
                        {
                            return false;
                        }
                    }
                }

                //se o fluxo sobreviver até aqui é pq os objetos complexos são equivalentes
                return true;
            }
            else
            {
                return IsEquivalentBasic(expected, actual);
            }
        }
    }
}
