﻿
using System;
using System.Collections.Generic;

namespace DesafioConcrete.Practices.Specification
{
    public class SpecificationResult
    {
        public bool IsSatisfied { get; private set; }

        private List<string> _messageCollection;
        public IEnumerable<string> MessageCollection { get { return _messageCollection; } }

        public SpecificationResult(bool isSatisfied, IEnumerable<string> messageCollection = null)
        {
            IsSatisfied = isSatisfied;

            if (messageCollection == null)
            {
                _messageCollection = new List<string>();
            }
            else
            {
                _messageCollection = new List<string>(messageCollection);
            }
        }

        public void AddMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentException("message must be defined.");

            _messageCollection.Add(message);
        }
    }
}
