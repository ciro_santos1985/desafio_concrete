﻿
namespace DesafioConcrete.Practices.Specification
{
    public interface ISpecification<T>
    {
        SpecificationResult IsSatisfiedBy(T businessObject);
    }
}
