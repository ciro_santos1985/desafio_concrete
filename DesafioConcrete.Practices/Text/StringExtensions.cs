﻿
using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DesafioConcrete.Practices.Text
{
    public static class StringExtensions
    {
        
        private static string DomainMapper(Match match)
        {
            var idn = new IdnMapping();
            string domainName = match.Groups[2].Value;

            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                return null;
            }

            return match.Groups[1].Value + domainName;
        }

        public static bool IsValidEmail(this string strIn)
        {
            try
            {
                if (string.IsNullOrEmpty(strIn))
                    return false;

                // Use IdnMapping class to convert Unicode domain names.
                var strInAux = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Return true if strIn is in valid e-mail format.
                return Regex.IsMatch(strInAux,
                     @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                     @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                     RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static string SecureString(this string input, HashAlgorithm hashAlg, Encoding strEncoding)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            if (hashAlg == null)
                throw new ArgumentNullException("hashAlg");

            if (strEncoding == null)
                throw new ArgumentNullException("strEncoding");

            var inputAsBytes = strEncoding.GetBytes(input);
            var hashedInput = hashAlg.ComputeHash(inputAsBytes);

            return Convert.ToBase64String(hashedInput);
        }
    }
}
