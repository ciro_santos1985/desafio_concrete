﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesafioConcrete.Practices.Commands
{
    public class CommandResult
    {
        public CommandResultStatus Status { get; private set; }

        private List<string> _messageCollection;

        public IEnumerable<string> MessageCollection { get { return _messageCollection; } }

        public CommandResult(CommandResultStatus status, IEnumerable<string> messages = null)
        {
            Status = status;

            if (messages == null)
            {
                _messageCollection = new List<string>();
            }
            else
            {
                _messageCollection = messages.ToList();
            }
        }

        public CommandResult(CommandResultStatus status, string message)
            : this(status, (IEnumerable<string>)null)
        {
            _messageCollection.Add(message);
        }

        public void AddMessage(string message)
        {
            _messageCollection.Add(message);
        }
    }
}
