﻿
namespace DesafioConcrete.Practices.Commands
{
    public enum CommandResultStatus
    {
        Success = 200,
        CreatedResource = 201,
        InvalidInputs = 400,
        Unauthorized = 401,
        AccessDenied = 403,
        ResourceNotFound = 404
    }
}
