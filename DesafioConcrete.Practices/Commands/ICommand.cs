﻿using System.Threading.Tasks;

namespace DesafioConcrete.Practices.Commands
{
    public interface ICommand
    {
        CommandResult Execute();
    }

    public interface IAsyncCommand : ICommand
    {
        Task<CommandResult> ExecuteAsync();
    }
}
