﻿using System;

namespace DesafioConcrete.Business.Model
{
    public class PhoneNumber
    {
        public Guid IdProfile { get; private set; }

        public string AreaCode { get; private set; }

        public string Number { get; private set; }

        public PhoneNumber(Guid idProfile, string areaCode, string number)
        {
            IdProfile = idProfile;
            AreaCode = areaCode;
            Number = number;
        }
    }
}
