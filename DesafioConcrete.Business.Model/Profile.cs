﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using DesafioConcrete.Practices.Text;
using System.Linq;

namespace DesafioConcrete.Business.Model
{
    public class Profile
    {
        private static Encoding _dataEncoding = Encoding.UTF8;

        private const string StrGuidFormat = "N";

        public Guid Id { get; private set; }

        public string Password { get; private set; }

        public DateTime CreationTime { get; private set; }

        public string AccessToken { get; private set; }

        private Dictionary<string, PhoneNumber> _phoneNumberCollection = new Dictionary<string, PhoneNumber>();
        public IEnumerable<PhoneNumber> PhoneNumberCollection
        {
            get
            {
                return _phoneNumberCollection.Values;
            }

            private set
            {
                _phoneNumberCollection = value.ToDictionary<PhoneNumber, string>(item => item.AreaCode + item.Number);
            }
        }

        public DateTime LastUpdateTime { get; set; }

        public DateTime LastLoginTime { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Profile()
        {
            Id = Guid.NewGuid();
            CreationTime = DateTime.Now;
        }

        public Profile(Guid profileId, DateTime dataCriacao, string hashedPass = null, string hashedToken = null)
        {
            Id = profileId;
            CreationTime = dataCriacao;
            Password = hashedPass;
            AccessToken = hashedToken;
        }

        public void AddPhoneNumber(PhoneNumber phoneNumber)
        {
            if (phoneNumber == null)
                throw new ArgumentNullException("phoneNumber");

            if (phoneNumber.AreaCode == null)
                throw new ArgumentException("Código de área deve estar definido.");

            if (phoneNumber.Number == null)
                throw new ArgumentException("Número do telefone deve estar definido");

            if (!Id.Equals(phoneNumber.IdProfile))
                throw new Exception("Telefone pertence a outro Perfil");

            var phoneKey = phoneNumber.AreaCode + phoneNumber.Number;
            const string errorMessageTemplate = "Telefone {0} {1} já cadastrado para o perfil.";

            if (_phoneNumberCollection.ContainsKey(phoneKey))
                throw new ArgumentException(string.Format(errorMessageTemplate, phoneNumber.AreaCode, phoneNumber.Number));

            _phoneNumberCollection.Add(phoneKey, phoneNumber);
        }

        public bool MatchPassword(string candidate)
        {
            if (string.IsNullOrEmpty(candidate))
                return false;

            using (var hashAlg = SHA256.Create())
            {
                var hashedCandidate = candidate.SecureString(hashAlg, _dataEncoding);
                return hashedCandidate == Password;
            }
        }

        public void ChangePassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Senha Inválida !");

            using (var hashAlg = SHA256.Create())
            {
                Password = password.SecureString(hashAlg, _dataEncoding);
            }
        }

        public bool MatchAccessToken(Guid token)
        {
            var candidate = token.ToString(StrGuidFormat);

            using (var hashAlg = SHA256.Create())
            {
                var hashedCandidate = candidate.SecureString(hashAlg, _dataEncoding);
                return hashedCandidate == AccessToken;
            }
        }

        public void ChangeAccessToken(Guid token)
        {
            var strToken = token.ToString(StrGuidFormat);

            using (var hashAlg = SHA256.Create())
            {
                AccessToken = strToken.SecureString(hashAlg, _dataEncoding);
            }
        }
    }
}
