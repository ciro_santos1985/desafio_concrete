﻿using DesafioConcrete.Data.Configs;
using DesafioConcrete.Data.DataObject;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;

namespace DesafioConcrete.Data
{
    public class EfSqliteDbContext : DbContext
    {
        public DbSet<ProfileDataObject> Profiles { get; set; }

        public DbSet<PhoneNumberDataObject> PhoneNumbers { get; set; }


        private static bool _databaseCreated = false;
        private static object _databaseCreatedSyncRoot = new object();

        private static string _createProfileTableCommand;
        private static string _createPhoneNumberTableCommand;

        static EfSqliteDbContext()
        {
            var sqlCommand = new StringBuilder();

            sqlCommand.Append("create table Profile ( ");
            sqlCommand.Append("Id text primary key asc, ");
            sqlCommand.Append("AccessToken text not null unique, ");
            sqlCommand.Append("Password text not null, ");
            sqlCommand.Append("DataCriacao text not null, ");
            sqlCommand.Append("DataAtualizacao text not null, ");
            sqlCommand.Append("DataUltimoLogin text not null, ");
            sqlCommand.Append("Name text not null, ");
            sqlCommand.Append("Email text not null unique);");

            _createProfileTableCommand = sqlCommand.ToString();
            sqlCommand.Clear();

            sqlCommand.Append("create table PhoneNumber( ");
            sqlCommand.Append("IdProfile text, ");
            sqlCommand.Append("AreaCode text, ");
            sqlCommand.Append("Number text, ");
            sqlCommand.Append("primary key (IdProfile, AreaCode, Number), ");
            sqlCommand.Append("FOREIGN KEY(IdProfile) REFERENCES Profile(Id));");

            _createPhoneNumberTableCommand = sqlCommand.ToString();
        }

        public EfSqliteDbContext()
            : base("localdb")
        {

            if (!_databaseCreated)
            {
                lock (_databaseCreatedSyncRoot)
                {
                    if (!_databaseCreated)
                    {
                        Database.ExecuteSqlCommand(_createProfileTableCommand);
                        Database.ExecuteSqlCommand(_createPhoneNumberTableCommand);

                        _databaseCreated = true;
                    }
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new ProfileMappingConfig());
            modelBuilder.Configurations.Add(new PhoneNumberMappingConfig());
        }
    }
}
