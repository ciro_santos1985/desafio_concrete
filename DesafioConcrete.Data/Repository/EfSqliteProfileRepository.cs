﻿
using System;
using System.Threading.Tasks;
using DesafioConcrete.Business.Model;
using DesafioConcrete.Data.Contracts;
using System.Data.Entity;
using DesafioConcrete.Data.DataObject;
using System.Linq;

namespace DesafioConcrete.Data.Repository
{
    public class EfSqliteProfileRepository : IProfileRepository
    {
        private EfSqliteDbContext _dbContext;

        public EfSqliteProfileRepository(EfSqliteDbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");

            _dbContext = dbContext;
        }

        private async Task LoadCompositionAsync(ProfileDataObject dataObject)
        {
            var phones = await _dbContext.PhoneNumbers.Where(p => p.IdProfile == dataObject.Id).ToListAsync().ConfigureAwait(false);
            dataObject.PhoneNumberCollection = phones;
        }
       
        public async Task<Profile> GetByEmailAsync(string email)
        {
            var dataObject = await _dbContext.Profiles.FirstOrDefaultAsync(p => p.Email == email).ConfigureAwait(false);

            if (dataObject == null)
                return null;

            await LoadCompositionAsync(dataObject).ConfigureAwait(false);
            return dataObject.CreateBusinessObject();
        }

        public Task InsertAsync(Profile profile)
        {
            if (profile == null)
                throw new ArgumentNullException("profile");

            //não é a prática recomendada do async mas está sendo utilizada para se obedecer a interface que se adequa a maioria dos casos
            //se a conexão estivesse sendo feita pelo ADO.NET o ideal seria mandar o comando de insert de forma assíncrona ao banco
            return Task.Run(() =>
            {
                var dataObject = new ProfileDataObject(profile);
                _dbContext.Profiles.Add(dataObject);
            });
        }

        public async Task<bool> UpdateAsync(Profile profile)
        {
            if (profile == null)
                throw new ArgumentNullException("profile");

            var profileId = profile.Id.ToString("N");
            var dataObject = await _dbContext.Profiles.FirstOrDefaultAsync(p => p.Id == profileId).ConfigureAwait(false);

            if (dataObject == null)
                return false;

            dataObject.UpdateDataObject(profile);
            _dbContext.Profiles.Attach(dataObject);

            var entry = _dbContext.Entry(dataObject);
            entry.State = EntityState.Modified;

            return true;
        }

        public async Task<Profile> GetByIdAsync(Guid profileId)
        {
            var strId = profileId.ToString("N");
            var dataObject = await _dbContext.Profiles.FirstOrDefaultAsync(p => p.Id == strId).ConfigureAwait(false);

            if (dataObject == null)
                return null;

            await LoadCompositionAsync(dataObject).ConfigureAwait(false);
            return dataObject.CreateBusinessObject();
        }
    }
}
