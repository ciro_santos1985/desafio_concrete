﻿using System;
using System.Threading.Tasks;
using DesafioConcrete.Data.Contracts;
using DesafioConcrete.Data.Repository;

namespace DesafioConcrete.Data
{
    public class EfSqliteUnitOfWork : IDesafioConcreteUnitOfWork
    {
        private EfSqliteDbContext _dbContext;

        private object _profileSyncRoot = new object();
        private IProfileRepository _profileRepository;

        public IProfileRepository ProfileRepository
        {
            get
            {
                if (_profileRepository == null)
                {
                    lock (_profileSyncRoot)
                    {
                        if (_profileRepository == null)
                        {
                            _profileRepository = new EfSqliteProfileRepository(_dbContext);
                        }
                    }
                }

                return _profileRepository;
            }
        }

        public EfSqliteUnitOfWork()
        {
            _dbContext = new EfSqliteDbContext();
        }

        public Task CommitAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
               
        public Task RollbackAsync()
        {
            return Task.Run(() =>
            {
                var transaction = _dbContext.Database.CurrentTransaction;

                if (transaction != null)
                {
                    transaction.Rollback();
                }
            });
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            _dbContext = null;
        }
    }
}
