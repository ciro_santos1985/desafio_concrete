﻿
namespace DesafioConcrete.Data.DataObject
{
    public interface IDataObject<T> where T : class
    {
        T CreateBusinessObject();
    }
}
