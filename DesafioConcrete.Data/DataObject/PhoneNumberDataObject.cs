﻿

using DesafioConcrete.Business.Model;
using System;

namespace DesafioConcrete.Data.DataObject
{
    public class PhoneNumberDataObject : IDataObject<PhoneNumber>
    {
        private const string _guidToStringFormat = "N";

        public string IdProfile { get; private set; }

        public string AreaCode { get; private set; }

        public string Number { get; private set; }

        public PhoneNumberDataObject() : base() { }

        public PhoneNumberDataObject(PhoneNumber phoneNumber)
        {
            if (phoneNumber == null)
                throw new ArgumentNullException("phoneNumber");

            IdProfile = phoneNumber.IdProfile.ToString(_guidToStringFormat);
            AreaCode = phoneNumber.AreaCode;
            Number = phoneNumber.Number;
        }

        public PhoneNumber CreateBusinessObject()
        {
            return new PhoneNumber(Guid.Parse(IdProfile), AreaCode, Number);
        }
    }
}
