﻿
using DesafioConcrete.Business.Model;
using System.Collections.Generic;
using System;
using System.Linq;

namespace DesafioConcrete.Data.DataObject
{
    public class ProfileDataObject : IDataObject<Profile>
    {
        private const string _guidFormat = "N";

        public string Id { get; set; }

        public string Password { get; set; }

        public string AccessToken { get; private set; }

        public string DataCriacao { get; private set; }

        public string DataAtualizacao { get; set; }

        public string DataUltimoLogin { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public ICollection<PhoneNumberDataObject> PhoneNumberCollection { get; set; }

        public ProfileDataObject() : base() { }

        public ProfileDataObject(Profile profile)
        {
            if (profile == null)
                throw new ArgumentNullException("profile");

            UpdateDataObject(profile);
        }

        internal void UpdateDataObject(Profile profile)
        {
            Id = profile.Id.ToString(_guidFormat);
            Password = profile.Password;

            DataCriacao = profile.CreationTime.ToString();
            DataAtualizacao = profile.LastUpdateTime.ToString();

            DataUltimoLogin = profile.LastLoginTime.ToString();
            AccessToken = profile.AccessToken;

            Name = profile.Name;
            Email = profile.Email;

            PhoneNumberCollection = profile.PhoneNumberCollection
                .Select(phone => new PhoneNumberDataObject(phone))
                .ToList();
        }

        public Profile CreateBusinessObject()
        {
            var prof = new Profile(Guid.Parse(Id), DateTime.Parse(DataCriacao), Password, AccessToken)
            {
                LastUpdateTime = DateTime.Parse(DataAtualizacao),
                LastLoginTime = DateTime.Parse(DataUltimoLogin),
                Email = Email,
                Name = Name
            };

            var phoneList = PhoneNumberCollection.ToList();

            foreach (var phone in phoneList)
            {
                prof.AddPhoneNumber(phone.CreateBusinessObject());
            }

            return prof;
        }
    }
}
