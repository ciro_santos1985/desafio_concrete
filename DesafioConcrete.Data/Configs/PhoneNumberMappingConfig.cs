﻿using DesafioConcrete.Data.DataObject;
using System.Data.Entity.ModelConfiguration;

namespace DesafioConcrete.Data.Configs
{
    public class PhoneNumberMappingConfig : EntityTypeConfiguration<PhoneNumberDataObject>
    {
        public PhoneNumberMappingConfig()
        {
            ToTable("PhoneNumber");

            Property(p => p.IdProfile);

            Property(p => p.AreaCode)
                .HasMaxLength(5);

            Property(p => p.Number)
                .HasMaxLength(15);
                        
            HasKey(p => new { p.IdProfile, p.AreaCode, p.Number });
        }
    }
}
