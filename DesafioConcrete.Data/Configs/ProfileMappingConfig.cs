﻿using DesafioConcrete.Data.DataObject;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace DesafioConcrete.Data.Configs
{
    public class ProfileMappingConfig : EntityTypeConfiguration<ProfileDataObject>
    {
        public ProfileMappingConfig()
        {
            ToTable("Profile");

            Property(p => p.Id);
            HasKey(p => p.Id);

            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(256);

            Property(p => p.Password)
                .IsRequired()
                .HasMaxLength(256);

            Property(p => p.DataAtualizacao)
                .IsRequired();

            Property(p => p.DataCriacao)
                .IsRequired();

            Property(p => p.DataUltimoLogin)
                .IsRequired();

            //índice unico para email
            var emailIndexAtt = new IndexAttribute("IX_Email_Profile") { IsUnique = true };
            var emailIndex = new IndexAnnotation(emailIndexAtt);

            Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, emailIndex);

            //índice único para o token
            var tokenIndexAtt = new IndexAttribute("IX_AccessToken_Profile") { IsUnique = true };
            var tokenIndex = new IndexAnnotation(tokenIndexAtt);

            Property(p => p.AccessToken)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, tokenIndex);
                        
            HasMany(p => p.PhoneNumberCollection)
                .WithRequired()
                .HasForeignKey(p => p.IdProfile);
        }
    }
}
