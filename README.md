Desafio Concrete Feito por Ciro Santos (ciro.santos@uol.com.br)

Desafio publicado no Amazon AWS:

Exemplo de Chamadas:

SignUP

POST http://desafio-concrete-ciro.us-east-1.elasticbeanstalk.com/api/profile

[Body] - {
 "nome": "Ciro",
  "email" : "ciro.santos@teste.com.br",
  "senha" : "123456",
  "telefones": [{"ddd":"21","numero":"8876545783"}]
}

Login 

PUT http://desafio-concrete-ciro.us-east-1.elasticbeanstalk.com/api/profile/auth

[Body] - {
   "email" : "ciro.santos@teste.com.br",
  "senha" : "123456"
}

Obter Perfil

GET http://desafio-concrete-ciro.us-east-1.elasticbeanstalk.com/api/profile/c5284dbc-6a6c-45e4-b149-948b62a4eac8 {Id do Perfil}

[Header] Authorizarion Bearer 81e2cadb-ce76-40c2-a50e-a9fdbe51b2b4 {token de acesso}