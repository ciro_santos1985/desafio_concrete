﻿
using DesafioConcrete.Business.Model;
using System;
using System.Threading.Tasks;

namespace DesafioConcrete.Data.Contracts
{
    public interface IProfileRepository
    {
        Task InsertAsync(Profile profile);

        Task<bool> UpdateAsync(Profile profile);

        Task<Profile> GetByEmailAsync(string email);

        Task<Profile> GetByIdAsync(Guid profileId);
    }
}
