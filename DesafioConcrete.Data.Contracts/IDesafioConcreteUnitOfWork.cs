﻿

using System;
using System.Threading.Tasks;

namespace DesafioConcrete.Data.Contracts
{
    public interface IDesafioConcreteUnitOfWork : IDisposable
    {
        IProfileRepository ProfileRepository { get; }
        Task CommitAsync();
        Task RollbackAsync();
    }
}
